from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_cors import CORS
import os

app = Flask(__name__)
CORS(app)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql://postgres:spurthi@localhost:5432/airlines"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
# init db
db = SQLAlchemy(app)
# init ma
ma = Marshmallow(app)


class airlines(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name_of_airlines = db.Column(db.String(200))
    alias = db.Column(db.String(100))
    iata = db.Column(db.String(10))
    icao = db.Column(db.String(20))
    callsign = db.Column(db.String(200))
    country = db.Column(db.String(100))
    active = db.Column(db.String(50))
    __table_args__ = {"schema":"aviation"}

    def __init__(
        self, id, name_of_airlines, alias, iata, icao, callsign, country, active
    ):
        self.id = id
        self.name_of_airlines = name_of_airlines
        self.alias = alias
        self.iata = iata
        self.icao = icao
        self.callsign = callsign
        self.country = country
        self.active = active


# airlines schema
class AirlinesSchema(ma.Schema):
    class Meta:
        fields = (
            "id",
            "name_of_airlines",
            "alias",
            "iata",
            "icao",
            "callsign",
            "country",
            "active",
        )


# init schema
airlines_schema = AirlinesSchema()
airliness_schema = AirlinesSchema(many=True)

# CREATE A AIRLINE


@app.route("/airlines", methods=["POST"])
def add_airlines():
    id = request.json["id"]
    name_of_airlines = request.json["name_of_airlines"]
    alias = request.json["alias"]
    iata = request.json["iata"]
    icao = request.json["icao"]
    callsign = request.json["callsign"]
    country = request.json["country"]
    active = request.json["active"]

    new_airlines = airlines(
        id, name_of_airlines, alias, iata, icao, callsign, country, active
    )

    db.session.add(new_airlines)
    db.session.commit()

    return airlines_schema.jsonify(new_airlines)

@app.route("/airlines/<id>", methods=["GET"])
def get_airline(id):
    airline = airlines.query.get(id)
    return airlines_schema.jsonify(airline)

@app.route("/airlines", methods=["GET"])
def get_airlines():
    all_airlines = airlines.query.all()
    result = airliness_schema.dump(all_airlines)
    return jsonify(result)

#Planes class db

class Planes(db.Model):
    name = db.Column(db.String(300),primary_key=True)
    icao = db.Column(db.String(50))
    iata = db.Column(db.String(50))
    __table_args__ = {"schema":"aviation"}

    def __init__(self,
        name, icao,iata):
        self.name = name
        self.icao = icao
        self.iata = iata

# planes schema
class PlaneSchema(ma.Schema):
    class Meta:
        fields = (
            "name",
            "icao",
            "iata"
        )


# init schema
plane_schema = PlaneSchema()
planes_schema = PlaneSchema(many=True)

# CREATE A plane data


@app.route("/planes", methods=["POST"])
def add_plane():
    name = request.json["name"]
    icao = request.json["icao"]
    iata = request.json["iata"]

    new_plane = Planes(
         name,icao, iata
    )

    db.session.add(new_plane)
    db.session.commit()

    return plane_schema.jsonify(new_plane)

@app.route("/planes", methods=["GET"])
def get_all_planes():
    all_name = Planes.query.all()
    result = planes_schema.dump(all_name)
    return jsonify(result)


# db Countries
class Countries(db.Model):
    name = db.Column(db.String(200),primary_key=True)
    iso_code = db.Column(db.String(50))
    dafif_code = db.Column(db.String(50))
    __table_args__ = {"schema":"aviation"}

    def __init__(self,
        name, iso_code,dafif_code):
        self.name = name
        self.iso_code = iso_code
        self.dafif_code = dafif_code

# countries schema
class CountrySchema(ma.Schema):
    class Meta:
        fields = (
            "name",
            "iso_code",
            "dafif_code"
        )


# init schema
country_schema = CountrySchema()
countries_schema = CountrySchema(many=True)

# CREATE A country


@app.route("/country", methods=["POST"])
def add_country():
    name = request.json["name"]
    iso_code = request.json["icao"]
    dafif_code = request.json["iata"]

    new_country = Countries(
         name,iso_code, dafif_code
    )

    db.session.add(new_country)
    db.session.commit()

    return country_schema.jsonify(new_country)

@app.route("/country", methods=["GET"])
def get_all_countries():
    all_name = Countries.query.all()
    result = countries_schema.dump(all_name)
    return jsonify(result)

# routes db

class Routes(db.Model):
    route_id = db.Column(db.Integer,primary_key=True)
    airline = db.Column(db.String(100))
    airline_id = db.Column(db.String(150))
    source_airport = db.Column(db.String(200))
    sourceairport_id = db.Column(db.String(200))
    destination_airport = db.Column(db.String(100))
    destination_airport_id = db.Column(db.String(150))
    codeshare = db.Column(db.String(20))
    stops =db.Column(db.String(200))
    equipment =db.Column(db.String(100))

    __table_args__ = {"schema":"aviation"}

    def __init__(self,route_id,
        airline,airline_id,source_airport,sourceairport_id,destination_airport,destination_airport_id,codeshare,stops,equipment):
        self.route_id = route_id
        self.airline =airline
        self.airline_id = airline_id
        self.source_airport = source_airport
        self.sourceairport_id=source_airport_id
        self.destination_airport=destination_airport
        self.destination_airport_id=destination_airport_id
        self.codeshare=codeshare
        self.stops=stops
        self.equipment=equipment

#routes schema

class RouteSchema(ma.Schema):
    class Meta:
        fields = (
            "route_id",
            "airline",
            "airline_id",
            "source_airport",
            "sourceairport_id",
            "destination_airport",
            "destination_airport_id",
            "codeshare",
            "stops",
            "equipment"
        )


# init schema
route_schema = RouteSchema()
routes_schema = RouteSchema(many=True)

# CREATE A route data


@app.route("/routes", methods=["POST"])
def add_route():
    route_id=request.json["route_id"]
    airline = request.json["airline"]
    airline_id = request.json["airline_id"]
    source_airport = request.json["source_airport"]
    sourceairport_id = request.json["sourceairport_id"]
    destination_airport = request.json["destination_airport_id"]
    destination_airport_id = request.json["destination_airport_id"]
    codeshare = request.json["codeshare"]
    stops = request.json["stops"]
    equipment = request.json["equipment"]

    new_route = Routes(
         route_id,airline,airline_id,source_airport,sourceairport_id,destination_airport,destination_airport_id,codeshare,stops,equipment
    )

    db.session.add(new_route)
    db.session.commit()

    return route_schema.jsonify(new_route)

@app.route("/routes", methods=["GET"])
def get_all_routes():
    all_route_id = Routes.query.all()
    result = routes_schema.dump(all_route_id)
    return jsonify(result)

# run server
if __name__ == "__main__":
    app.run(debug=True)
